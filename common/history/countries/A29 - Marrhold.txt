﻿COUNTRIES = {
	c:A29 = {
		effect_starting_technology_tier_4_tech = yes	#doc says lagging behind rest of Escann in terms of institutions and economy
		add_technology_researched = line_infantry		#behind but adding some bits as they are still militarized
		add_technology_researched = army_reserves
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_professional_army

		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_traditional_magic_encouraged
	

	}
}