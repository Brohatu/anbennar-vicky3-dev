﻿COUNTRIES = {
	c:B02= {
		effect_starting_technology_tier_3_tech = yes
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_national_guard
	}
}