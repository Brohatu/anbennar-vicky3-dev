﻿COUNTRIES = {
	c:E08 = {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = centralization
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
	}
}