﻿DIPLOMATIC_PLAYS = {
	c:B91 = { #Ranger Republic
		create_diplomatic_play = {
			name = second_great_skirmish
		
			target_state = s:STATE_ELATHAEL.region_state:B37
			
			war = yes
			
			type = dp_annex_war

			add_war_goal = {
				holder = c:B37 #Elathael
				type = conquer_state
				target_state = s:STATE_ELATHAEL.region_state:B37
			}
			add_war_goal = {
				holder = c:B37 #Elathael
				type = conquer_state
				target_state = s:STATE_ARANTAS.region_state:B37
			}
		}
	}
}