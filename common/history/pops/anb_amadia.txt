﻿POPS = {
	s:STATE_BRONRIN = {
		region_state:C02 = {
			create_pop = {
				culture = west_damerian
				size = 100
			}
			create_pop = {
				culture = east_damerian
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 10
			}
		}
	}
	s:STATE_NUR_DAMENATH = {
		region_state:C02 = {
			create_pop = {
				culture = east_damerian
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 10
			}
		}
	}
	s:STATE_MUNASIN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
			create_pop = {
				culture = east_damerian
				size = 100
			}
			create_pop = {
				culture = west_damerian
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
		}
	}
	s:STATE_SILVEGOR = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
		}
	}
	s:STATE_CLAMGUINN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = pearlsedger
				size = 10
			}
			create_pop = {
				culture = doos
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 100
			}
		}
	}
	s:STATE_URANCESTIR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
		}
	}
	s:STATE_CALILVAR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 100
			}
		}
	}
	s:STATE_CYMBEAHN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 100
			}
		}
		region_state:C11 = {
			create_pop = {
				culture = doosun
				size = 100
			}
		}
	}
	s:STATE_OOMU_NELIR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = doos
				size = 100
			}
		}
		region_state:C11 = {
			create_pop = {
				culture = doos
				size = 100
			}
		}
	}
	s:STATE_IMELAINE = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = doosun
				size = 100
			}
		}
		region_state:C11 = {
			create_pop = {
				culture = doos
				size = 10
			}
			create_pop = {
				culture = doosun
				size = 100
			}
		}
		region_state:C15 = {
			create_pop = {
				culture = effe_i
				size = 100
			}
		}
	}
	s:STATE_HARENAINE = {
		region_state:C11 = {
			create_pop = {
				culture = doosun
				size = 100
			}
		}
	}
	s:STATE_ARANLAS = {
		region_state:C11 = {
			create_pop = {
				culture = doosun
				size = 100
			}
		}
	}
}