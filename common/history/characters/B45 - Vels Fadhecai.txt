﻿CHARACTERS = {
	c:B45 = {
		create_character = { #actually half-ynnic; daughter of general Camovac Konyrzab and mercenary Arok yen Clovenvels
			first_name = Varhilya
			last_name = Konyrzab
			historical = yes
			ruler = yes
			female = yes
			age = 44
			interest_group = ig_landowners
			ideology = ideology_moderate
			traits = {
				cautious ambitious experienced_political_operator
			}
		}
		create_character = { #the Jester Knight; banished from the Sarda Empire, could be sent back as agitator to destablize them. Or may die in an Epednar ambush...
			first_name = Fasendir
			last_name = yen_Cestor
			historical = yes
			age = 28
			is_general = yes
			interest_group = ig:ig_intelligentsia
			ideology = ideology_humanitarian
			traits = {
				reckless romantic
			}
		}
	}
}
