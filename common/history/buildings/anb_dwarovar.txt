﻿BUILDINGS = {
	s:STATE_HJYRTSIKKI = {
		region_state:A10 = {
			create_building = {
				building= "building_barracks"
				level= 3
				reserves= 1
				activate_production_methods={ "pm_wound_dressing" "pm_line_infantry" "pm_cavalry_scouts" "pm_no_specialists" "pm_cannon_artillery" }
			}
		}
	}
	s:STATE_FRIGID_FOREST = {
		region_state:A10 = {
			create_building = {
				building = "building_barracks"
				level = 10
				reserves = 1
				activate_production_methods = { "pm_wound_dressing" "pm_line_infantry" "pm_cavalry_scouts" "pm_no_specialists" "pm_cannon_artillery" }
			}
			create_building = {
				building = "building_textile_mills"
				level= 2
				reserves= 1
				activate_production_methods = { "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building = {
				building = "building_logging_camp"
				level = 10
				reserves = 1
				activate_production_methods = { "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building = {
				building = "building_rye_farm"
				level = 2
				reserves = 1
				activate_production_methods = { "pm_simple_farming" "pm_privately_owned" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building = {
				building = "building_livestock_ranch"
				level = 4
				reserves = 1
			}
		}
	}
	s:STATE_COPPERWOOD = {
		region_state:A10 = {
			create_building = {
				building="building_barracks"
				level = 10
				reserves = 1
				activate_production_methods={ "pm_wound_dressing" "pm_line_infantry" "pm_cavalry_scouts" "pm_no_specialists" "pm_cannon_artillery" }
			}
			create_building={
				building = "building_logging_camp"
				level = 4
				reserves = 1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building = "building_rye_farm"
				level = 1
				reserves = 1
				activate_production_methods={ "pm_simple_farming" "pm_privately_owned" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building = {
				building= "building_livestock_ranch"
				level= 2
				reserves= 1
			}
		}
	}

	s:STATE_KHUGSVALE = {
		region_state:A10 = {
			create_building = {
				building = "building_barracks"
				level = 10
				reserves = 1
				activate_production_methods = { "pm_wound_dressing" "pm_line_infantry" "pm_cavalry_scouts" "pm_no_specialists" "pm_cannon_artillery" }
			}
			create_building = {
				building = "building_logging_camp"
				level = 2
				reserves = 1
				activate_production_methods = { "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building = {
				building = "building_rye_farm"
				level = 3
				reserves = 1
				activate_production_methods = { "pm_simple_farming" "pm_privately_owned" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_LONELY_MOUNTAIN = {
		region_state:A10 = {
		}
	}
}