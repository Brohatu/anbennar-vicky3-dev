﻿STATE_VAGKEYSA = {
    id = 752
    subsistence_building = "building_subsistence_farms"
    provinces = { "x134218" "x402CFF" "x413040" "x413800" "x443FD7" "x59D38A" "x7E2B20" "xC12E00" "xC13280" "xC136FF" "xC13A40" "xD73F60" }
    traits = {}
    city = "x134218"
    farm = "x134218"
    mine = "x134218"
    wood = "x134218"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MIUMUD = {
    id = 756
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4784C0" "x478800" "x4890FF" "x544080" "xC78280" "xC786FF" "xC78A40" "xC89200" "xCA2EAD" }
    traits = {}
    city = "x4784C0"
    farm = "x4784C0"
    mine = "x4784C0"
    wood = "x4784C0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KHETERAT = {
    id = 757
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4354FF" "x445840" "x445CC0" "x446000" "x478C80" "x94A960" "xC35600" "xC45A80" "xC45EFF" }
    traits = {}
    city = "x4354FF"
    farm = "x4354FF"
    mine = "x4354FF"
    wood = "x4354FF"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {}
}
STATE_GOLKORA = {
    id = 758
    subsistence_building = "building_subsistence_farms"
    provinces = { "x426442" "x446000" "x446480" "x4570C0" "x6CFE68" "x897871" "x90EB90" "xC45EFF" "xC46240" "xC56A00" "xC572FF" }
    traits = {}
    city = "x426442"
    farm = "x426442"
    mine = "x426442"
    wood = "x426442"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_WEST_SALAHAD = {
    id = 759
    subsistence_building = "building_subsistence_farms"
    provinces = { "x16F2EF" "x307473" "xFD3007" }
    traits = {}
    city = "x16F2EF"
    farm = "x16F2EF"
    mine = "x16F2EF"
    wood = "x16F2EF"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BOOZHUR = {
    id = 760
    subsistence_building = "building_subsistence_farms"
    provinces = { "x07D9D7" "x2BF6D1" "x90DFEB" "xF2B170" }
    traits = {}
    city = "x07D9D7"
    farm = "x07D9D7"
    mine = "x07D9D7"
    wood = "x07D9D7"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BAGUNAZ = {
    id = 761
    subsistence_building = "building_subsistence_farms"
    provinces = { "x112BCE" "x8D10DC" }
    traits = {}
    city = "x112BCE"
    farm = "x112BCE"
    mine = "x112BCE"
    wood = "x112BCE"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BAZUGHU = {
    id = 762
    subsistence_building = "building_subsistence_farms"
    provinces = { "x08A57D" "x1ABC27" "x3B77A7" "x46196D" "x4A2C7E" "x6DE01F" "x821FC9" "xA01FC1" "xBB46BF" "xCAAAC6" }
    traits = {}
    city = "x08A57D"
    farm = "x08A57D"
    mine = "x08A57D"
    wood = "x08A57D"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BUBUKARR = {
    id = 763
    subsistence_building = "building_subsistence_farms"
    provinces = { "x38371F" "x3A8D1D" "x4C6D77" "x6C596D" "x9FD5C2" "xA355DB" "xADFA2A" "xB6FBB0" "xB89446" "xC96152" "xCA91BD" "xE13CF3" "xE46BE3" "xF19D40" }
    traits = {}
    city = "x38371F"
    farm = "x38371F"
    mine = "x38371F"
    wood = "x38371F"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_SIGILAN = {
    id = 764
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0FE562" "x72C872" "x8053CF" "xA2C029" "xCA69D0" "xD51E33" "xE109D6" }
    traits = {}
    city = "x0FE562"
    farm = "x0FE562"
    mine = "x0FE562"
    wood = "x0FE562"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BINWARJI = {
    id = 765
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2114B5" "x23D7A9" "x5B836B" "x5E5957" "x60F8F9" "x74C9EF" "x8EC6C8" "x979B8E" "xDDD227" "xE404B7" }
    traits = {}
    city = "x2114B5"
    farm = "x2114B5"
    mine = "x2114B5"
    wood = "x2114B5"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KING_GRAHAMS_LANDING = {
    id = 766
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0634CE" "x36674E" "x95EAE0" "xEEFE1D" }
    traits = {}
    city = "x0634CE"
    farm = "x0634CE"
    mine = "x0634CE"
    wood = "x0634CE"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_FORT_WHITTINGTON = {
    id = 767
    subsistence_building = "building_subsistence_farms"
    provinces = { "x118DA0" "x5F5216" "xD50177" }
    traits = {}
    city = "x118DA0"
    farm = "x118DA0"
    mine = "x118DA0"
    wood = "x118DA0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_YELLOW_PORT = {
    id = 768
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4B7555" "x81A00A" "xC24284" "xE1818F" }
    traits = {}
    city = "x4B7555"
    farm = "x4B7555"
    mine = "x4B7555"
    wood = "x4B7555"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BENBA_KULU = {
    id = 769
    subsistence_building = "building_subsistence_farms"
    provinces = { "x454931" "x5ED129" "x816697" "x81970D" "x87F127" "xA87790" "xAA90C3" "xCB0B37" }
    traits = {}
    city = "x454931"
    farm = "x454931"
    mine = "x454931"
    wood = "x454931"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_NINKA_NANKA_CASTLE = {
    id = 770
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01626E" "xA3E618" "xC92242" }
    traits = {}
    city = "x01626E"
    farm = "x01626E"
    mine = "x01626E"
    wood = "x01626E"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_FOJARA = {
    id = 771
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01B5B5" "x15E08E" "x3840AC" "x5CCC3A" "x6B0256" "x788441" "x90D93C" "x9FC041" "xC17A9D" "xC783E4" "xD84418" "xE4D6D8" "xEF8267" "xFA62D3" "xFF85AC" }
    traits = {}
    city = "x01B5B5"
    farm = "x01B5B5"
    mine = "x01B5B5"
    wood = "x01B5B5"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_CASTLE_CORIN = {
    id = 772
    subsistence_building = "building_subsistence_farms"
    provinces = { "x04B2D1" "x6B44AE" "x6C3E4B" "x809DEE" "xB7BE3B" }
    traits = {}
    city = "x04B2D1"
    farm = "x04B2D1"
    mine = "x04B2D1"
    wood = "x04B2D1"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_NEGEFAAMA = {
    id = 773
    subsistence_building = "building_subsistence_farms"
    provinces = { "x284448" "x65D5E2" "xA4CD71" "xB9E6E2" "xDB0635" "xF606AC" "xF6DD71" }
    traits = {}
    city = "x284448"
    farm = "x284448"
    mine = "x284448"
    wood = "x284448"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_PORT_NARAWAN = {
    id = 774
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0ACDB0" "x6A44FA" "x844F75" "xB5C06D" "xCCE2BF" "xEA14B5" }
    traits = {}
    city = "x0ACDB0"
    farm = "x0ACDB0"
    mine = "x0ACDB0"
    wood = "x0ACDB0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KUNOLO = {
    id = 775
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BF31C" "x326F0C" "x563E68" "xA9C212" "xD12002" "xECE967" }
    traits = {}
    city = "x0BF31C"
    farm = "x0BF31C"
    mine = "x0BF31C"
    wood = "x0BF31C"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KHAWIT = {
    id = 776
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10C231" }
    traits = {}
    city = "x10C231"
    farm = "x10C231"
    mine = "x10C231"
    wood = "x10C231"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_ZEEWIX = {
    id = 777
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1CEFA1" "x46AD97" "x94559F" "xB36790" "xD7EBC9" "xF180F1" }
    traits = {}
    city = "x1CEFA1"
    farm = "x1CEFA1"
    mine = "x1CEFA1"
    wood = "x1CEFA1"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MANTIMATU = {
    id = 778
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1D6E66" "x20383B" "x31FF0F" "x3AC9A6" "x6E920C" "x766E25" "x845A29" "x96D2C7" "xA7F81F" "xB94138" "xCAF494" "xE47886" }
    traits = {}
    city = "x1D6E66"
    farm = "x1D6E66"
    mine = "x1D6E66"
    wood = "x1D6E66"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VENERG_RAX = {
    id = 779
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4568FF" "x456C40" "xC566C0" "xC56E80" "xF02189" }
    traits = {}
    city = "x4568FF"
    farm = "x4568FF"
    mine = "x4568FF"
    wood = "x4568FF"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KAA_HAVA = {
    id = 780
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D380C" "x1F77AA" "x373654" "x526D71" "xB47CA0" "xD9ED30" "xE4F01B" }
    traits = {}
    city = "x0D380C"
    farm = "x0D380C"
    mine = "x0D380C"
    wood = "x0D380C"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_DHAL_NIKHUVAD = {
    id = 900
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E2C7E" "x637DB2" "x92E66F" "xD22CA7" "xF94F81" }
    traits = {}
    city = "x637DB2"
    farm = "x637DB2"
    mine = "x637DB2"
    wood = "x637DB2"
    arable_land = 69
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_DHEBIJ_JANAB = {
    id = 901
    subsistence_building = "building_subsistence_farms"
    provinces = { "x371F8E" "x56F9EC" "x842FCB" "x87F71A" }
    traits = {}
    city = "x56F9EC"
    farm = "x56F9EC"
    mine = "x56F9EC"
    wood = "x56F9EC"
    arable_land = 63
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_SUHRATBA_YAJ = {
    id = 902
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x0962A6" "x159F03" "x39FA16" "x6E50DC" "x7B0511" }
    traits = {}
    city = "x159F03"
    farm = "x159F03"
    mine = "x159F03"
    wood = "x159F03"
    arable_land = 48
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_QASRIYINGI = {
    id = 903
    subsistence_building = "building_subsistence_farms"
    provinces = { "x046160" "x54A798" "x64EEAC" "x809D71" "xBF990C" "xE0C160" }
    traits = {}
    city = "x64EEAC"
    farm = "x64EEAC"
    mine = "x64EEAC"
    wood = "x64EEAC"
    arable_land = 76
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_BAASHI_BADDA = {
    id = 904
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x14F888" "x25E624" "x394174" "x53A1EB" "xC6F466" }
    traits = {}
    city = "x25E624"
    farm = "x25E624"
    mine = "x25E624"
    wood = "x25E624"
    arable_land = 52
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_SUHRATBA_YAHIN = {
    id = 905
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x22FB02" "x23DAFA" "x512A4E" "xF88745" }
    traits = {}
    city = "xF88745"
    farm = "xF88745"
    mine = "xF88745"
    wood = "xF88745"
    arable_land = 34
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_DHAL_TANIZUUD = {
    id = 906
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BFAB5" "x7B5A09" "x7F137D" "xDAB2E8" }
    traits = {}
    city = "xDAB2E8"
    farm = "xDAB2E8"
    mine = "xDAB2E8"
    wood = "xDAB2E8"
    arable_land = 59
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_JURITAQA = {
    id = 907
    subsistence_building = "building_subsistence_farms"
    provinces = { "x32E2D3" "x7A8026" "xB69B54" }
    traits = {}
    city = "x7A8026"
    farm = "x7A8026"
    mine = "x7A8026"
    wood = "x7A8026"
    arable_land = 108
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_MPAKA = {
    id = 908
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3D48BC" "x6CEA94" "xF2EFEA" }
    traits = {}
    city = "xF2EFEA"
    farm = "xF2EFEA"
    mine = "xF2EFEA"
    wood = "xF2EFEA"
    arable_land = 74
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_DEBIJ_SHAR = {
    id = 909
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0CBC46" "x2F700F" "x35042C" "x61C75E" "x736D8D" "xC977E5" }
    traits = {}
    city = "x736D8D"
    farm = "x736D8D"
    mine = "x736D8D"
    wood = "x736D8D"
    arable_land = 72
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_ASHAMAD_BARIGA = {
    id = 910
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0C7167" "x888DF8" "xBA14D0" "xD98049" "xF14D7F" }
    traits = { state_trait_good_soils }
    city = "x0C7167"
    farm = "x0C7167"
    mine = "x0C7167"
    wood = "x0C7167"
    arable_land = 149
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_DHAI_BAEIDAG = {
    id = 911
    subsistence_building = "building_subsistence_farms"
    provinces = { "x8F515C" "xB19085" "xE8C9F9" }
    traits = {}
    city = "xE8C9F9"
    farm = "xE8C9F9"
    mine = "xE8C9F9"
    wood = "xE8C9F9"
    arable_land = 71
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_QASRI_ABEESOOYINKA = {
    id = 912
    subsistence_building = "building_subsistence_farms"
    provinces = { "x12C30C" "x23B901" "x3D0E08" "x48CB0C" "x6477B7" "x896517" "xE936A7" "xEBCB1F" }
    traits = { state_trait_good_soils state_trait_alnamaliu_estuary }
    city = "x896517"
    farm = "x896517"
    mine = "x896517"
    wood = "x896517"
    arable_land = 213
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 16
    }
}

STATE_QAYNSLAND = {
    id = 913
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02A0ED" "x4DC795" "x67B72F" "x6B35A3" "x89017B" "x9D99B5" "xBDF7C5" "xCDB694" "xE3773D" }
    traits = {}
    city = "x67B72F"
    farm = "x67B72F"
    mine = "x67B72F"
    wood = "x67B72F"
    arable_land = 78
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
}

STATE_THE_OHITS = {
    id = 914
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2D93E0" "x5B1BD8" "xA703FF" "xB34FB5" "xD6BD94" "xDE0DDA" "xE27C89" "xE4403D" "xF75DB2" }
    traits = { state_trait_natural_harbors }
    city = "xB34FB5"
    farm = "xB34FB5"
    mine = "xB34FB5"
    wood = "xB34FB5"
    arable_land = 82
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
}

STATE_HARENMARCHES = {
    id = 915
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3DE367" "x560617" "x718F0D" "x8FE18C" "xCDE994" "xDC74C9" }
    traits = {}
    city = "xCDE994"
    farm = "xCDE994"
    mine = "xCDE994"
    wood = "xCDE994"
    arable_land = 53
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_SAMAANIA = {
    id = 916
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BDF26" "x2187DF" "x680D38" "x6B834D" }
    traits = {}
    city = "x6B834D"
    farm = "x6B834D"
    mine = "x6B834D"
    wood = "x6B834D"
    arable_land = 61
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
}

STATE_ELIANDE = {
    id = 917
    subsistence_building = "building_subsistence_farms"
    provinces = { "x635EA2" "x739AD2" "x996B08" "xB06E77" }
    traits = { state_trait_deep_ardimyan_desert }
    city = "xB06E77"
    farm = "xB06E77"
    mine = "xB06E77"
    wood = "xB06E77"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
    }
}

STATE_HADEADOL = {
    id = 918
    subsistence_building = "building_subsistence_farms"
    provinces = { "x08606B" "x46C83A" "x6FC76B" "x9CC8D6" "xAA4D42" "xC6667E" "xCB8D15" "xF7EAB1" }
    traits = {}
    city = "xC6667E"
    farm = "xC6667E"
    mine = "xC6667E"
    wood = "xC6667E"
    arable_land = 64
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_HARASCILDE = {
    id = 920
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1C09ED" "x5F6263" "x7406B0" "x8A5057" "xC1BD3D" "xC63F33" "xC88E52" "xD1FD51" "xE557A5" "xEC0A7C" }
    traits = {}
    city = "xC88E52"
    farm = "xC88E52"
    mine = "xC88E52"
    wood = "xC88E52"
    arable_land = 57
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
}

STATE_OLD_JINNAKAH = {
    id = 921
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0452B0" "x397C06" "x49247E" "xB3D418" "xC25143" "xC9E1DE" }
    traits = { state_trait_deep_ardimyan_desert }
    city = "xC25143"
    farm = "xC25143"
    mine = "xC25143"
    wood = "xC25143"
    arable_land = 5
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
    }
}

STATE_SSIPPANSEK = {
    id = 922
    subsistence_building = "building_subsistence_farms"
    provinces = { "x102158" "x9EAD2A" "xC974B1" "xDFBCEA" }
    traits = {}
    city = "xC974B1"
    farm = "xC974B1"
    mine = "xC974B1"
    wood = "xC974B1"
    arable_land = 84
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 20
    }
}
