﻿STATE_LOKEMEION = {
    id = 700
    subsistence_building = "building_subsistence_farms"
    provinces = { "x7A3061" "x863569" "xB2428C" "xD18BB9" "xD52654" }
    traits = {}
    city = "x7a3061" #Torexyl
























    farm = "xd18bb9" #Sothan
























    wood = "xd52654" #Basothoam
























    port = "x863569" #Lokemeion
























    mine = "xb2428c" #Agoxetei
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_ORMAM_KHERKA = {
    id = 701
    subsistence_building = "building_subsistence_farms"
    provinces = { "x597C52" "x599BFE" "x916505" "x97BEC2" "x9853BA" "xBA8B00" "xCC4F62" }
    traits = {}
    city = "x97bec2" #Kherka
























    farm = "x916505" #Sidlar
























    wood = "xd52654" #Basothoam
























    port = "xba8b00" #Ormam
























    mine = "x599bfe" #Coskheis
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_DEGAKHEION = {
    id = 702
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3A937C" "x476491" "x528080" "x7C3800" "xC15B1F" "xC64A69" "xFDFF9E" }
    traits = {}
    city = "xc15b1f" #Metedeyam
























    farm = "x476491" #Agholor
























    wood = "x3a937c" #Promethe
























    port = "xfdff9e" #Degakheion
























    mine = "x528080" #Daidyam
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_OKTIKHEION = {
    id = 703
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1AD874" "x26686D" "x2D6B28" "x3094DA" "x35FF4D" "x78376F" "x92427E" "xAA537C" "xFF653F" }
    traits = {}
    city = "x2d6b28" #Kimanis
























    farm = "x92427e" #Koinago
























    wood = "x1ad874" #Kheinos
























    port = "x35ff4d" #Oktikheion
























    mine = "xff653f" #Astolion
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_silk_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_ARPEDIFER = {
    id = 704
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2EBA92" "x5D6B25" "x74A674" "x862C7C" "x93781F" "xA638CE" "xFF8D47" }
    traits = {}
    city = "x74a674" #Lora
























    farm = "x862c7c" #Gemelos
























    wood = "x93781f" #Gakorochos
























    port = "x5d6b25" #Arpedifer
























    mine = "x2eba92" #Mtelian
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_ELTIKAN = {
    id = 705
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03C480" "x4408B3" "x44E6B3" "x485023" "x502080" "x5664FF" "x7ED82E" "xB7FF8E" "xBA2EB0" "xBC4343" "xCE982D" "xDBAE5C" "xE723B3" "xFF9999" }
    traits = {}
    city = "xff9999" #Tirberana
























    farm = "xba2eb0" #Sarkiona
























    wood = "x44e6b3" #Urartola
























    port = "xbc4343" #Gonomoro
























    mine = "x4408b3" #Tinideoro
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 4553
}

STATE_VOTHELISI_ARPENISI = {
    id = 706
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x2052A3" "x52CEAB" "x5DBC54" "x778011" "x7C3F3E" "x83C677" "xB2C649" "xDDD468" }
    traits = {}
    city = "x778011" #Serigastisi
























    farm = "x83c677" #Somelisi
























    wood = "x52ceab" #Vothelisi
























    port = "x7c3f3e" #Anisigo
























    mine = "xb2c649" #Cosan
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_ANISIKHEION = {
    id = 707
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x4C769B" "x89FF7C" "xC9DB2B" "xEAE36B" "xF1CF81" }
    traits = {}
    city = "xc9db2b" #Anisikheion
























    farm = "xf1cf81" #Agonisi
























    wood = "xeae36b" #Anikhein
























    port = "x89ff7c" #Anisoton
























    mine = "x4c769b" #Proylasi
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_EMPKEIOS = {
    id = 708
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B335F" "x191FF6" "x204091" "x21C698" "x32832E" "x4A0FF6" "x5F7A2E" "x6C1FC4" "x6F1F5F" "x7183C4" "x891C2E" "x96E7C4" "x9E725F" "xB0B5F6" "xC81FC4" "xDA1A5F" "xDD90F6" "xEF1291" }
    traits = {}
    city = "x891c2e" #Dhanam
























    farm = "xda1a5f" #Sigam
























    wood = "x96e7c4" #Vorlidir
























    port = "x7183c4" #Empkeios
























    mine = "x4a0ff6" #Rinigos
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_sugar_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_KEYOLION = {
    id = 709
    subsistence_building = "building_subsistence_farms"
    provinces = { "x6F3D5F" "x7F96FF" "xC466C4" "xED1F2E" "xEDE72E" "xFF59B9" "xFFE45E" }
    traits = {}
    city = "x" #Proga
























    farm = "x" #Lokeyogen
























    wood = "xffe45e" #Phineion
























    port = "x" #Keyolion
























    mine = "x7f96ff" #Eionetei
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_ENEION = {
    id = 710
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1D2677" "x33FF28" "x3CD639" "x57CC59" "x698777" "xC0652E" "xEA5656" "xFF6B1C" }
    traits = {}
    city = "xff6b1c" #Trilidir
























    farm = "xea5656" #Eltanedio
























    wood = "x57cc59" #Tritheiam
























    port = "x3cd639" #Eneion
























    mine = "x698777" #Lartor
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_BESOLAKI = {
    id = 711
    subsistence_building = "building_subsistence_farms"
    provinces = { "x157568" "x30377F" "x378247" "x6ABA50" "x6B4264" "x9692B8" "xB52D51" "xC5FF35" "xD67183" "xFF3D8A" }
    traits = {}
    city = "x378247" #Apikhoxi
























    farm = "x6b4264" #Gapoueion
























    wood = "xff3d8a" #Basorekos
























    port = "x30377f" #Besolaki
























    mine = "x9692b8" #Akrexyl
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4552
}

STATE_AMGREMOS = {
    id = 712
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2CD638" "x41DB58" "x4F827D" "x5C3BEF" "x68105B" "x82401D" "x88D294" "xA3463A" "xA3CF3A" "xA57475" "xD47392" "xD843C9" "xDBD3A4" "xFF4B42" }
    traits = {}
    city = "x4f827d" #Kangoara
























    farm = "x41db58" #Kykhaban
























    wood = "x68105b" #Msar Rtharkha
























    port = "x5c3bef" #Dnagutakh
























    mine = "xff4b42" #Dborhmey
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4554
}

STATE_MTEIDHAN = {
    id = 713
    subsistence_building = "building_subsistence_farms"
    provinces = { "x687CF6" "x689E52" "x811F91" "x83B76F" "x844C6C" "xAF4190" "xD83A22" }
    traits = {}
    city = "x689e52" #Denabatha
























    farm = "xd83a22" #Den Al Nsakh
























    wood = "xaf4190" #Pasraba
























    mine = "x687cf6" #Srthan Zhutash
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MTEIBHARA = {
    id = 714
    subsistence_building = "building_subsistence_farms"
    provinces = { "x060F5F" "x14832E" "x2D832E" "x301FC4" "x3B84F6" "x420D5F" "x451FF6" "x571F91" "x84102E" "xAB1FF6" "xAEE791" "xB9E3C4" "xC0C92E" "xC31FC4" "xD5D55F" "xD81FF6" "xE3C92E" "xEA1F91" "xF8F85F" }
    traits = {}
    city = "xd81ff6" #Mzhaara
























    farm = "xab1ff6" #Bacaeuaes
























    wood = "x14832e" #Kanziyen
























    mine = "x301fc4" #Hfaesban
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_DEYEION = {
    id = 715
    subsistence_building = "building_subsistence_farms"
    provinces = { "x031FC4" "x262CC4" "x290F5F" "x3E2D91" "x5377C4" "x5A1F2E" "x651F5F" "x6836F6" "x7D1F2E" "x825582" "x9292F6" "x991F5F" "xA40C91" "xA7252E" "xBCBF5F" "xC66253" "xCE9AF6" "xD1DB91" "xD8319E" "xE61FC4" "xF01FC4" "xFBBCF6" }
    traits = {}
    city = "x5377c4" #Oktiamoton
























    farm = "x3e2d91" #Phadilor
























    wood = "x7d1f2e" #Koeal
























    port = "xa40c91" #Deyeion
























    mine = "x290f5f" #Volithoram
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4554
}

STATE_AMEION = {
    id = 716
    subsistence_building = "building_subsistence_farms"
    provinces = { "x118391" "x1D29F6" "x381F5F" "x501F2E" "x5979F6" "x5C2D91" "x779AF6" "x7AE891" "x8F135F" "x9B1FC4" "xA183F6" "xB6C92E" "xD702C4" "xF51FC4" }
    traits = {}
    city = "x1d29f6" #Haebysziund
























    farm = "x8f135f" #Heztwanysar
























    wood = "x501f2e" #Khrghaana
























    port = "x118391" #Ameyban
























    mine = "x779af6" #Tzkheban
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4554
}

STATE_CHENDHYA = {
    id = 717
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0879C4" "x1A035F" "x23DD2E" "x2C83F6" "x2F0191" "x4401C4" "x47C25F" "x4D8491" "x6E012E" "x861F91" "x98012E" "xADC75F" "xC2D691" "xC5C52E" "xCB295F" "xEC33F6" }
    traits = {}
    city = "x47c25f" #Nopanais
























    farm = "xcb295f" #Budar Gaednae
























    wood = "x23dd2e" #Xartraelas
























    mine = "x98012e" #Dongaednae
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_CLEMATAR = {
    id = 718
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05CA2E" "x3F77C4" "x696D2E" "x6B1F91" "x7E015F" "x8365F6" "xA8975F" "xBDDD91" "xE70FF6" "xE96F5F" "xF2FF2E" "xFC1F2E" "xFC832E" }
    traits = {}
    city = "x696d2e" #Lodhrim
























    farm = "x7e015f" #Svirsi Vindar
























    wood = "x05ca2e" #Pedhrim
























    mine = "xfc832e" #Genhi Varamesh
























    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_OLD_LARANKAR = {
    id = 719
    subsistence_building = "building_subsistence_farms"
    provinces = { "x162091" "x2585B4" "x2B33C4" "x4077F6" "x43C991" "x6A0C5F" "x7F1491" "x8282C4" "x94853C" "x94A15F" "xA90291" "xBD7991" "xBE64C4" "xD31FF6" "xE8C92E" }
    traits = {}
    city = "x2b33c4" #Enlarmai
























    farm = "x43c991" #Sithra Calindash
























    wood = "x8282c4" #Imarti Sadvya
























    mine = "x4077f6" #Ethra
























    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_ARPEION = {
    id = 720
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D1FC4" "x41852E" "x4C015F" "x568D5F" "x80B55F" "x8B0B91" "x957A91" "xD4D72E" }
    traits = {}
    city = "x4c015f" #Varanya Sandaman
























    farm = "x957a91" #Dongyccyl
























    wood = "x0d1fc4" #Tayakorrim
























    port = "x8b0b91" #Korrimutren
























    mine = "x80b55f" #Enretvil
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4554
}

STATE_MARHED_VARANYA = {
    id = 721
    subsistence_building = "building_subsistence_farms"
    provinces = { "x028391" "x3D1F5F" "x6715C4" "x7C83F6" "xA61FF6" "xC89A4F" "xC926AD" "xD0C75F" "xE51F91" "xFA8DC4" }
    traits = {}
    city = "xa61ff6" #Banderuttai
























    farm = "x7c83f6" #Thekavasanikkunnu
























    wood = "x6715c4" #Varaendi
























    mine = "xd0c75f" #Kattisangamar
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_NANRU_NAKAR = {
    id = 722
    subsistence_building = "building_subsistence_farms"
    provinces = { "x19BF2E" "x1CABC4" "x283D2E" "x2EE75F" "x521F91" "x5826C4" "x5B025F" "x6D29F6" "x91FFC4" "x97A1F6" "x9AF391" "xC18B5F" }
    traits = {}
    city = "x2ee75f" #Nakar
























    farm = "x6d29f6" #Varamkq
























    wood = "x19bf2e" #Kattvyavaram
























    mine = "x283d2e" #Uesrenti
























    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_NAGAR_VYECHEI = {
    id = 723
    subsistence_building = "building_subsistence_farms"
    provinces = { "x171FC4" "x34B791" "x37832E" "x738D2E" "xB2035F" "xF165F6" }
    traits = {}
    city = "xb2035f" #Andital
























    farm = "xf165f6" #Anakalchend
























    wood = "x37832e" #Telinkad
























    port = "x34b791" #Nagar Vyechei
























    mine = "x738d2e" #Varanyachend
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4554
}

STATE_KLERECHEND = {
    id = 724
    subsistence_building = "building_subsistence_farms"
    provinces = { "x227AF6" "x3A01C4" "x610C91" "xA0DDC4" "xA3BF5F" "xB81F91" "xCACA2E" "xDF845F" }
    traits = {}
    city = "xa3bf5f" #Sthanan it Vussam
























    farm = "xcaca2e" #Kannalulthe
























    wood = "x3a01c4" #Bayasidaphoriha
























    mine = "x610c91" #Gophira
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_SIBISIMRA = {
    id = 725
    subsistence_building = "building_subsistence_farms"
    provinces = { "x250691" "x64032E" "x79A15F" "xCD01C4" "xE229F6" "xF7832E" }
    traits = {}
    city = "xe229f6" #Nymbhava
























    farm = "x64032e" #Uddachend
























    wood = "xcd01c4" #Sibisimra
























    mine = "x250691" #Matnadevela
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VUL_TENVACH = {
    id = 726
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10135F" "x4970C4" "x5E30F6" "x657D36" "x7683C4" "x8E1F2E" "x9DA42E" "xB5C9F6" "xC7E891" "xDC2CC4" "xF4E791" }
    traits = {}
    city = "x8e1f2e" #Vul Tenvach
























    farm = "xc7e891" #Korrimadhala
























    wood = "x9da42e" #Curiyankad
























    port = "x5e30f6" #Thekvussam
























    mine = "x7683c4" #Uyarbaid
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4555
}

STATE_ORENKORAIM = {
    id = 727
    subsistence_building = "building_subsistence_farms"
    provinces = { "x078391" "x318DF6" "x3B5225" "x46A12E" "x70A491" "x85E75F" "x889EF6" "xAF2BC4" "xD6E791" "xEB98C4" "xEEF45F" "xFF7E32" }
    traits = {}
    city = "x078391" #Kalavend
























    farm = "x318df6" #Cankamam
























    wood = "xeb98c4" #Troppaimkq
























    port = "xaf2bc4" #Palaya Orenkoraim
























    mine = "xeef45f" #Entislula
























    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 4555
}

STATE_VEYII_SIKARHA = {
    id = 728
    subsistence_building = "building_subsistence_farms"
    provinces = { "x176565" "x228B71" "x2657AC" "x36AC26" "x5EB375" "x7C5EB3" "xAC2696" "xAED3B6" "xB36C5E" "xB3B35E" }
    traits = {}
    city = "xb36c5e" #RANDOM
























    farm = "xac2696" #RANDOM
























    wood = "X228b71" #RANDOM
























    mine = "X176565" #RANDOM
























    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KAYDHANO_SEA = {
    id = 729
    subsistence_building = "building_subsistence_farms"
    provinces = { "x296B5A" "x391F1C" "x6B3E29" "x74839B" "x749B7F" "x9B2A1D" "xC78EAD" }
    impassable = { "xc78ead" "x296b5a" "x9b2a1d" "x6b3e29" "x391f1c" "x749b7f" "x74839b" }
    traits = {}
    city = "xc78ead" #RANDOM
























    farm = "x296b5a" #RANDOM
























    wood = "x9b2a1d" #RANDOM
























    mine = "x6b3e29" #RANDOM
























    arable_land = 0
}

STATE_AINECUTIR = {
    id = 730
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2A0C91" "x3583C4" "x5483F6" "x7C0500" "x93972E" "xB3DB91" "xD20BC4" }
    traits = {}
    city = "xd20bc4" #RANDOM
























    farm = "x5483f6" #RANDOM
























    wood = "x2a0c91" #RANDOM
























    mine = "x3583c4" #RANDOM
























    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
